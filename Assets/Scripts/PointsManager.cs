﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsManager : MonoBehaviour
{
    public Text coinText;
    public Text moneyText;

    void Start()
    {
        PlayerData.Money = 0;
        PlayerData.Coins = 0;
        UpdateTextBar();
    }

    public void AddMoney(int count)
    {
        PlayerData.Money += count;
        UpdateTextBar();
    }

    public void ReduceMoney(int count)
    {
        PlayerData.Money -= count;
        UpdateTextBar();
    }

    public void AddCoins(int count)
    {
        PlayerData.Coins += count;
        UpdateTextBar();
    }

    public void ReduceCoins(int count)
    {
        PlayerData.Coins -= count;
        UpdateTextBar();
    }

    public void UpdateTextBar()
    {
        coinText.text = PlayerData.Coins.ToString();
        moneyText.text = PlayerData.Money.ToString();
    }
}