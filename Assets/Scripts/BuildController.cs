﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildController : MonoBehaviour
{
    public float speedRotate;

    void Update()
    {
        transform.Rotate(0, speedRotate, 0);
    }
}