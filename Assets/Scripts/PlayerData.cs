﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerData
{
    private static int coins;

    private static int moneys;
    public static int Coins 
    {
        get
        {
            return coins;
        }

        set
        {
            if (value < 0)
                coins = 0;
            else
                coins = value;
        }
    }

    public static int Money
    {
        get
        {
            return moneys;
        }

        set
        {
            if (value < 0)
                moneys = 0;
            else
                moneys = value;
        }
    }

    public static string PlayerName { get; set; }

    public static string CompanyName { get; set; }
}